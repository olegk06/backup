//
//  NSTimeInterval.swift
//  NFLMobile
//
//  Created by Oleg Koshkin on 11/1/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation

public extension NSTimeInterval {
    static let SecondsInMinute      = 60.0
    static let SecondsInHour        = 3_600.0
    static let SecondsInDay         = 86_400.0
    static let SecondsInWeek        = 604_800.0
    static let MinutesInHour        = 60.0
    static let HoursInDay           = 24.0
    static let DaysInWeek           = 7.0
    
    var seconds: Int {
        return Int(self % NSTimeInterval.SecondsInMinute)
    }
    
    var minutes: Int {
        return Int((self / NSTimeInterval.SecondsInMinute) % NSTimeInterval.MinutesInHour)
    }
    
    var hours: Int {
        return Int((self / NSTimeInterval.SecondsInHour) % NSTimeInterval.HoursInDay)
    }
    
    var asMinutes: Int {
        return Int(self / NSTimeInterval.SecondsInMinute)
    }
    
    var asHours: Int {
        return Int(self / NSTimeInterval.SecondsInHour)
    }
    
    var asDays: Int {
        return Int(self / NSTimeInterval.SecondsInDay)
    }
    
    func dayComponents() -> (seconds: Int, minutes: Int, hours: Int, days: Int) {
        let s = self.seconds
        let m = self.minutes
        let h = self.hours
        let d = self.asDays
        return (seconds: s, minutes: m, hours: h, days: d)
    }
}
