//
//  AlertExtension.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 12/26/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import UIKit

extension UIViewController {
    
    public func showAlert(message: String){
        showAlert(message, title: "Error")
    }
    
    public func showAlert(message: String, title: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    public func showLocationDisabledAlert(){
        showAlert("To use location you must turn on 'Always' in the Location Services Settings", title: "Location services are off")
    }

    
    public func addActivityIndicator(activityIndicatorStyle style: UIActivityIndicatorViewStyle = .Gray) -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: style)
        activityIndicator.center = self.view.center
        self.view.addSubview(activityIndicator)
        self.view.bringSubviewToFront(activityIndicator)
        return activityIndicator
    }
}
