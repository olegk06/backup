//
//  NotificationViewCell.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/15/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import UIKit


class NotificationViewCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var actionTitle: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var locations: UILabel!
}
