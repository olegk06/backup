//
//  Model.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/10/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import ObjectMapper

public class Model: Mappable {
    
    // MARK: - Mappable
    
    public init(){}
    
    public required init?(_ map: Map) {
        mapping(map)
    }
    
    public func mapping(map: Map) {
        
    }
}
