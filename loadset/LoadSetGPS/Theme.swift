//
//  ThemeService.swift
//  SnapperLite
//
//  Created by Alex Troyanskij on 11/12/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import UIKit

public class Theme {
    
    public static let service = Theme()
    
    public let navigationBarBarTintColor = UIColor(netHex: 0x155abb)
    public let navigationBarTitleTextColor = UIColor.whiteColor()
    public let navigationBarTintColor = UIColor.whiteColor()
    
    public let tabBarBarTintColor = UIColor(netHex: 0xf5f5f5)
    public let tabBarTintColor = UIColor(netHex: 0x03a9f4)
    public let tabBarItemSelectedColor = UIColor(netHex: 0x03a9f4)
    public let tabBarItemColor = UIColor(netHex: 0x757575)
    public let segmentedControlSelectedColor = UIColor(netHex: 0x739cd6)
    public let segmentedControlBackgroundColor = UIColor(netHex: 0x447bc9)
    public let segmentedControlTextColor = UIColor.whiteColor()
    
    public let textColorDark = UIColor(netHex: 0x212121)
    public let textColorGrey = UIColor(netHex: 0x757575)
    public let textColorLightGrey = UIColor(netHex: 0x9e9e9e)
    public let textColorBlue = UIColor(netHex: 0x03a9f4)
    public let textColorSelected = UIColor(netHex: 0xffffff)
    
    public let dividerColor = UIColor(netHex: 0xe0e0e0)
    
    public let gradientStartColor = UIColor(netHex: 0x04a3f0)
    public let gradientEndColor = UIColor(netHex: 0x0961c1)
    
    public let baseFont = UIFont.systemFontOfSize(14)
    public let baseBoldFont = UIFont.boldSystemFontOfSize(14)
    public let dateFont = UIFont.systemFontOfSize(10)
    public let secondaryTitleFont = UIFont.systemFontOfSize(12)
    
    public let cellSelectedColor = UIColor(netHex: 0x03a9f4)
    
    
    func setupBasicUI() {
        UINavigationBar.appearance().translucent = false
        UISegmentedControl.appearance().backgroundColor = segmentedControlBackgroundColor
        UISegmentedControl.appearance().tintColor = segmentedControlSelectedColor
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : segmentedControlTextColor], forState: .Normal)
        UISegmentedControl.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : segmentedControlTextColor], forState: .Selected)
        
        UINavigationBar.appearance().barTintColor = navigationBarBarTintColor
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : navigationBarTitleTextColor]
        UINavigationBar.appearance().tintColor = navigationBarTintColor
        
        UITabBar.appearance().barTintColor = tabBarBarTintColor
        UITabBar.appearance().tintColor = tabBarItemSelectedColor
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : tabBarItemSelectedColor], forState: .Selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : tabBarItemColor], forState: .Normal)
    }    
    
    func gradientBackgroundLayer(frame: CGRect) -> CAGradientLayer {
        return gradientLayer([Theme.service.gradientStartColor.CGColor, Theme.service.gradientEndColor.CGColor], frame: frame)
    }
    
    func gradientLayer(colors: [CGColor], frame: CGRect) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = colors
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.frame = frame
        return gradient
    }
}
