//
//  RawRepresentableTransform.swift
//  
//  Created by Oleg Koshkin on 11/1/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import ObjectMapper

public class RawRepresentableTransform<T: RawRepresentable>: TransformType {
    public typealias Object = T
    public typealias JSON = T.RawValue

    public init() {}

    public func transformFromJSON(value: AnyObject?) -> T? {
        if let rawValue = value as? T.RawValue {
            return T(rawValue: rawValue)
        }
        return nil
    }

    public func transformToJSON(value: T?) -> T.RawValue? {
        if let value = value {
            return value.rawValue
        }
        return nil
    }
}
