//
//  AverageStatisticCellView.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/10/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import UIKit

class AverageStatisticViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var milesLabel: UILabel!
    @IBOutlet weak var drivingLabel: UILabel!
    @IBOutlet weak var idleLabel: UILabel!
    
    func setData(data: (String, String, String, String)){
        dateLabel.textColor = Theme.service.textColorDark
        milesLabel.textColor = Theme.service.textColorDark
        drivingLabel.textColor = Theme.service.textColorDark
        idleLabel.textColor = Theme.service.textColorDark
        
        dateLabel.text = data.0
        milesLabel.text = data.1
        drivingLabel.text = data.2
        idleLabel.text = data.3
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.backgroundColor = Theme.service.tabBarBarTintColor
        
        if selected {
            let bgColorView = UIView()
            bgColorView.backgroundColor = Theme.service.cellSelectedColor
            self.selectedBackgroundView = bgColorView
            
            dateLabel.textColor = Theme.service.textColorSelected
            milesLabel.textColor = Theme.service.textColorSelected
            drivingLabel.textColor = Theme.service.textColorSelected
            idleLabel.textColor = Theme.service.textColorSelected
        } else {
            dateLabel.textColor = Theme.service.textColorDark
            milesLabel.textColor = Theme.service.textColorDark
            drivingLabel.textColor = Theme.service.textColorDark
            idleLabel.textColor = Theme.service.textColorDark
        }
    }
}
