//
//  MVVMControllerProtocol.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/24/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation

public protocol MVVMControllerProtocol {

    func setViewModel(viewModel: ViewModel)->Void
}
