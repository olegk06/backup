//
//  ViewModelExtension.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 1/5/16.
//  Copyright © 2016 Realine. All rights reserved.
//

import Foundation
import MapKit

extension ViewModel {
    
    func updateLocation(handler: (_:CLLocation?)->Void){
        if CLLocationManager.authorizationStatus() == .AuthorizedAlways {
            _ = Tracker.service.currentLocation
                .take(1)
                .filter{ $0 != nil }
                .takeUntil(didBecomeInactive)
                .subscribeNext{
                    handler($0)
                    if !Tracker.service.active {
                        Tracker.service.stopUpdatingLocation()
                    }
            }
        } else if CLLocationManager.authorizationStatus() == .NotDetermined {
            _ = Tracker.service.authorizationStatus
                .flatMap{ _ in Tracker.service.currentLocation }
                .take(1)
                .filter{ $0 != nil }
                .takeUntil(didBecomeInactive)
                .subscribeNext{
                    handler($0)
                    if !Tracker.service.active {
                        Tracker.service.stopUpdatingLocation()
                    }
            }
            Tracker.service.requestAlwaysAuthorization()
        } else {
            handler(nil)
        }
    }
}

