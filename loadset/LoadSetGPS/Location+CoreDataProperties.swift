//
//  Location+CoreDataProperties.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/28/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Location {

    @NSManaged var latitude: Double
    @NSManaged var longitude: Double
    @NSManaged var saved: Bool
    @NSManaged var timestamp: NSTimeInterval
    @NSManaged var speed: Double
    @NSManaged var altitude: Double
    @NSManaged var accuracy: Double
    @NSManaged var satelliteCount: Int32
    @NSManaged var route: Route?

}
