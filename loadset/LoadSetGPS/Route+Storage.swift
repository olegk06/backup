//
//  Route+Storage.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/2/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import CoreData
import LoginSignUpiOS

extension Route{
    static func fetchRoutes(context: NSManagedObjectContext) -> [Route]{
        let fetchRequest = NSFetchRequest(entityName: "Route")
        fetchRequest.predicate = NSPredicate(format: "user == %@", RLUserObject.instance().email)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "startTime", ascending: true)]
        
        do {
            let results = try context.executeFetchRequest(fetchRequest)
            return results as! [Route]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return [Route]()
        }
    }
    
    static func addRoute(context: NSManagedObjectContext) -> Route?{
        do {
            let entity =  NSEntityDescription.entityForName("Route",
                inManagedObjectContext:context)
            
            let route = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext: context)
            (route as! Route).user = RLUserObject.instance().email
            try context.save()
            return route as? Route
        } catch let error as NSError {
            print("Could not create route \(error), \(error.userInfo)")
            return nil
        }
    }
    
    static func save(context: NSManagedObjectContext){
        do {
            try context.save()
        } catch let error as NSError {
            print("Could save \(error), \(error.userInfo)")
        }
    }
    
    func fetchUnsavedLocations() -> [Location]? {
        return self.locations?.allObjects.map{ $0 as! Location }.filter{ !$0.saved }.sort{ $0.timestamp < $1.timestamp }
    }
    
    static func delete(route route: Route, context: NSManagedObjectContext){
        do {
            context.deleteObject(route)
            try context.save()
        } catch let error as NSError {
            print("Could save \(error), \(error.userInfo)")
        }
    }
}
