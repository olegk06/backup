//
//  Config.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 12/8/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import UIKit

private let AppNotificationsKey = "AppNotifications"
private let GPSBlackOutZoneEntryKey = "GPSBlackOutZoneEntry"
private let UnitsOfMeasureKey = "UnitsOfMeasure"
private let GPSEnabledKey = "GPSEnabled"

public let BlackoutZonesCancelNotification = "BlackoutZonesCancelNotification"

public enum UnitsOfMeasure: Int {
    case English
    case Metric
    
    
    static let defaultUnit: UnitsOfMeasure =
        (NSLocale.currentLocale().objectForKey(NSLocaleUsesMetricSystem) as! Bool) ? .Metric : .English
    
    var distanceDescription: String {
        switch self {
        case .English:
            return "Miles"
        case .Metric:
            return "Km"
        }
    }
    
    var distanceAbbr: String {
        switch self {
        case .English:
            return "mi"
        case .Metric:
            return "km"
        }
    }
    
    var defZoneRadius: Double {
        switch self {
        case .English:
            return 8.04672 // 5 miles
        case .Metric:
            return 5 // 5 km
        }
    }
}

public class Config {
    
    public static let service = Config()
    
    private var defaults: NSUserDefaults {
        return NSUserDefaults.standardUserDefaults()
    }
    
    public var appNotificationsEnabled: Bool {
        get {
            if let on: Bool = defaults.valueForKey(AppNotificationsKey) as? Bool {
                return on
            }
            return false
        }
        set {
            defaults.setValue(newValue, forKey: AppNotificationsKey)
            defaults.synchronize()
        }
    }
    
    public var gpsBlackoutZoneNotificationsEnabled: Bool {
        get {
            if let on: Bool = defaults.valueForKey(GPSBlackOutZoneEntryKey) as? Bool {
                if !on {
                    NSNotificationCenter.defaultCenter().postNotificationName(BlackoutZonesCancelNotification, object: nil)
                }
                return on
            }
            NSNotificationCenter.defaultCenter().postNotificationName(BlackoutZonesCancelNotification, object: nil)
            return false
        }
        set {
            defaults.setValue(newValue, forKey: GPSBlackOutZoneEntryKey)
            defaults.synchronize()
            if newValue {
                let app = UIApplication.sharedApplication()
                if app.currentUserNotificationSettings()?.types != [.Sound, .Alert, .Badge] {
                    app.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Sound, .Alert, .Badge], categories: nil))                    
                }
                
            }
        }
    }
    
    public var units: UnitsOfMeasure {
        get {
            if let value = defaults.valueForKey(UnitsOfMeasureKey) as? Int {
                return UnitsOfMeasure(rawValue: value)!
            }
            return UnitsOfMeasure.defaultUnit
        }
        set {
            defaults.setObject(newValue.rawValue, forKey: UnitsOfMeasureKey)
            defaults.synchronize()
        }
    }
    
    public var gpsEnabled: Bool {
        get {
            if let on: Bool = defaults.valueForKey(GPSEnabledKey) as? Bool {
                return on
            }
            return false
        }
        set {
            defaults.setValue(newValue, forKey: GPSEnabledKey)
            defaults.synchronize()
        }
    }
    

}
