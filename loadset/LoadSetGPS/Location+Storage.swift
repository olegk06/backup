//
//  Location+Storage.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/2/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import CoreLocation
import CoreData


let isSimulator = Device().isOneOf(Device.allSimulators)

extension Location {
    
    class func save(locations: [CLLocation], route: Route?, context: NSManagedObjectContext){
        locations.forEach { managedLocation($0, route: route, context: context)}
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save locations \(error), \(error.userInfo)")
        }
    }
    
    class func previous(loc: Location?, context: NSManagedObjectContext) -> Location?{
        guard let location = loc else {
            return nil
        }

        let fetchRequest = NSFetchRequest(entityName: "Location")
        let predicateRoute = NSPredicate(format: "route == %@", location.route! )
        let predicateTime = NSPredicate(format: "timestamp < %@",  NSDate(timeIntervalSinceReferenceDate: location.timestamp))
        fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicateRoute, predicateTime])
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "timestamp", ascending: false)]
        fetchRequest.fetchLimit = 1
        
        do {
            let results = try context.executeFetchRequest(fetchRequest)
            return results.first as! Location?
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
    }
    
    private class func managedLocation(location: CLLocation, route: Route?, context: NSManagedObjectContext) -> Location{
        let entity =  NSEntityDescription.entityForName("Location", inManagedObjectContext:context)
        let managedLocation = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: context) as! Location
        
        managedLocation.latitude = location.coordinate.latitude
        managedLocation.longitude = location.coordinate.longitude
        managedLocation.timestamp = location.timestamp.timeIntervalSinceReferenceDate
        managedLocation.speed = location.speed
        managedLocation.altitude = location.altitude
        managedLocation.accuracy = sqrt(location.horizontalAccuracy*location.horizontalAccuracy + location.verticalAccuracy*location.verticalAccuracy)
        managedLocation.route = route
        managedLocation.satelliteCount = Int32(sateliteCount(location))
        
        return managedLocation
    }
    
    private class func sateliteCount(location: CLLocation) -> Int {
        if isSimulator {
            return 6
        }
        
        if location.verticalAccuracy >= 0 {
            switch location.horizontalAccuracy {
            case 0..<10:
                return 6
            case 10..<30:
                return 5
            case 30..<50:
                return 3
            case 50..<100:
                return 2
            default:
                return 1
                
            }
        }
        return 1
    }
}
