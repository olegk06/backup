//
//  ConversionExtension.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 12/9/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation

extension Double {
    
    func normalize() -> Double {
        switch Config.service.units {
        case .English:
            return Conversions().kilometersToMiles(self)
        case .Metric:
            return self
        }
    }
}


