//
//  DataHelper.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/2/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import RxSwift
import CoreData
import RxBlocking

public class DataHelper{
    
    public static func newObjects() -> Observable<[AnyObject]>{
        return NSNotificationCenter.defaultCenter()
            .rx_notification(NSManagedObjectContextDidSaveNotification, object: nil)
            .map { $0.userInfo }
            .filter{ $0 != nil}
            .map{ $0![NSInsertedObjectsKey] as! NSSet }
            .map{ $0.allObjects }
    }
    
    public static func newLocations() -> Observable<[Location]>{
        return newObjects().map{ $0.filter{ $0 is Location} as! [Location] }
    }
    
}
