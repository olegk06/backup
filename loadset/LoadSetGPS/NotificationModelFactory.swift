//
//  NotificationModelFactory.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 1/26/16.
//  Copyright © 2016 Realine. All rights reserved.
//

import Foundation
import ObjectMapper

public enum EventCode: String {
    case userRegistered = "USER_REGISTERED"
}

public class NotificationModelFactory {
    public func createNotification(notificationItem: NotificationItem) -> RealineNotification {
        guard let _ = notificationItem.eventCode else {
            return RealineNotification()
        }
        
        switch notificationItem.eventCode! {
        case EventCode.userRegistered.rawValue:
            return userRegisteredModel(notificationItem)
        default:
            return RealineNotification()
        }
    }
    
    private func userRegisteredModel(notificationItem: NotificationItem) -> NewUserNotification {
        guard let jsonString = notificationItem.jsonData else {
            return NewUserNotification()
        }
        
        let notification = Mapper<NewUserNotification>().map(jsonString.stringByReplacingOccurrencesOfString("\\", withString: ""))
        
        guard let _ = notification else {
            return NewUserNotification()
        }
        
        return notification!
    }
}
