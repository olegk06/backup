//
//  Tracker.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 10/31/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa
import CoreData
import CoreMotion
import SwiftDate

let ENABLE_SYNC = true // for Debug purpose

public class Tracker: NSObject, CLLocationManagerDelegate{
    
    public static let service = Tracker()
    
    private static let minDistanceFilter = 15.0
    private static let minHeadingFilter = 15.0
    private static let syncInterval = 3.minutes
    
    private let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    private var regions: [CLCircularRegion] = []
    
    private var authStatus: Disposable?
    private var locationUpdates: Disposable?
    private var headingUpdates: Disposable?
    private var sync: Disposable?
    private var monitoringRegion: Disposable?
    private var currentRoute: Route?
    private lazy var locationManager: CLLocationManager! = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        manager.activityType = .AutomotiveNavigation
        manager.distanceFilter = minDistanceFilter
        manager.headingFilter = minHeadingFilter
        if #available(iOS 9, *) {
            manager.allowsBackgroundLocationUpdates = true
        }
        manager.pausesLocationUpdatesAutomatically = false
        return manager
    }()
    
    private let automotiveStateSubject = BehaviorSubject<Bool>(value:true)
    private lazy var activityManager = CMMotionActivityManager()
    private let isMotionAvailiable = CMMotionActivityManager.isActivityAvailable()
    
    public var active = false
    
    public var authorizationStatus: Observable<CLAuthorizationStatus> {
        return locationManager.rx_didChangeAuthorizationStatus
    }
    
    public func requestAlwaysAuthorization(){
        self.locationManager.requestAlwaysAuthorization()
    }

    public func stopUpdatingLocation(){
        self.locationManager.stopUpdatingLocation()
    }
    
    public var currentLocation: Observable<CLLocation?>{
        self.locationManager.startUpdatingLocation()
        self.locationManager.distanceFilter = 1
        
        return self.locationManager.rx_didUpdateLocations.flatMap{ Observable.just($0.last) }
    }
    
    override init() {
        super.init()
        updateBlackoutZones()
        
        _ = NSNotificationCenter.defaultCenter()
            .rx_notification(BlackoutZonesChangedNotification, object: nil)
            .subscribeNext{ _ in self.updateBlackoutZones() }
        
        _ = NSNotificationCenter.defaultCenter()
            .rx_notification(BlackoutZonesCancelNotification, object: nil)
            .subscribeNext{ _ in UIApplication.sharedApplication().cancelAllLocalNotifications() }
    }
    
    public func start(){
        
        if !active {
            Config.service.gpsEnabled = true
            currentRoute = createNewRoute()
            
            if ENABLE_SYNC {
                let scheduler = ConcurrentDispatchQueueScheduler(globalConcurrentQueueQOS: .Default)
                sync = Observable<Int>.interval(Tracker.syncInterval, scheduler: scheduler)
                    .subscribeNext{ _ in self.syncData() }
            }
            
            if CLLocationManager.authorizationStatus() == .AuthorizedAlways {
                subscribeToUpdates()
            } else if CLLocationManager.authorizationStatus() == .NotDetermined {
                self.locationManager.requestAlwaysAuthorization()
                authStatus = locationManager.rx_didChangeAuthorizationStatus
                    .subscribeNext { status in
                        if (status == .AuthorizedAlways){
                            self.subscribeToUpdates()
                        }
                }
            }
            
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
            
            monitoringRegion = self.locationManager.rx_didEnterRegion.subscribeNext{ _ in self.currentRoute = self.createNewRoute() }
            
            if isMotionAvailiable {
                activityManager.startActivityUpdatesToQueue(NSOperationQueue.currentQueue()!, withHandler: { (motion: CMMotionActivity?) -> Void in
                    let confidence = motion?.confidence == .High
                    let notAutomotive = !(motion?.automotive ?? false)
                    self.automotiveStateSubject.onNext( !(confidence && notAutomotive) )
                })
            }
        }
    }
    
    public func stop(){
        if active {
            Config.service.gpsEnabled = false
            sync?.dispose()
            self.active = false
            if isMotionAvailiable {
                activityManager.stopActivityUpdates()
            }
            locationManager.stopUpdatingLocation()
            locationManager.stopUpdatingHeading()
            authStatus?.dispose()
            locationUpdates?.dispose()
            monitoringRegion?.dispose()
            self.currentRoute?.stopTime = NSDate().timeIntervalSinceReferenceDate
            Route.save(self.context)
            syncData()
        }
    }
    
    private func updateBlackoutZones(){
        _ = API.getList(.ZonesList).observeOn(MainScheduler.instance)
            .subscribeNext { (list: [BlackoutZoneModel]) -> Void in
                self.startMonitoringZones(list)
        }
    }
    
    private func startMonitoringZones(zones:[BlackoutZoneModel]){
        self.regions = zones.filter{ $0.centerLongitude != nil && $0.centerLongitude != nil }
            .filter{ $0.radiusKm != nil && $0.name != nil } // compiler produces error when put all conditions to a single filter
            .filter{ $0.radiusKm! * 1000 < locationManager.maximumRegionMonitoringDistance }
            .map{ CLCircularRegion(center: CLLocationCoordinate2DMake($0.centerLatitude!, $0.centerLongitude!), radius: $0.radiusKm! * 1000, identifier: $0.name!) }
        
        self.locationManager.monitoredRegions.forEach{ self.locationManager.stopMonitoringForRegion($0) }
        self.regions.forEach{ self.locationManager.startMonitoringForRegion($0) }
        
        if (Config.service.gpsBlackoutZoneNotificationsEnabled){
            let app = UIApplication.sharedApplication()
            app.cancelAllLocalNotifications()
            self.regions.forEach{ region in
                let notification = UILocalNotification()
                notification.alertBody = "You have entered to \(region.identifier) blackout zone"
                notification.regionTriggersOnce = true
                notification.region = region
                app.scheduleLocalNotification(notification)
            }
        }
    }
    
    
    private func subscribeToUpdates() {
        self.active = true
        self.locationUpdates =
            Observable.combineLatest(locationManager.rx_didUpdateLocations, automotiveStateSubject.distinctUntilChanged()){ ($0, $1) }
            .observeOn(Dependencies.sharedDependencies.backgroundWorkScheduler)
            .filter{ $0.1 }.map{ $0.0.last }.filter{ $0 != nil }.map{ $0! } // -> Location!
            .filter{ NSDate().difference($0.timestamp, unitFlags: .Second).second < 60 }
            .doOn(onNext: { loc in
                let newFilter = self.filter(loc)
                if (abs(newFilter - self.locationManager.distanceFilter) > 20) {
                    self.locationManager.distanceFilter = newFilter
                }
            }).filter{ loc in self.regions.filter{ $0.containsCoordinate(loc.coordinate) }.count == 0 }
            .subscribeNext{
                Location.save([$0], route: self.currentRoute, context: self.context)
        }
        
        self.headingUpdates = self.locationManager.rx_didUpdateHeading
            .observeOn(Dependencies.sharedDependencies.backgroundWorkScheduler)
            .subscribeNext{ _ in
            if (self.locationManager.distanceFilter > Tracker.minDistanceFilter) {
                self.locationManager.distanceFilter = Tracker.minDistanceFilter
            }
        }
    }
    
    private func filter(location: CLLocation) -> CLLocationDistance {
        switch location.speed{
        case 0..<10:
            return 15
        case 10..<20:
            return 150
        case 25..<100:
            return 300
        default:
            return 10
        }
    }
    
    private func syncData(){
        let routes = Route.fetchRoutes(context)
        routes.forEach{ self.uploadRoute($0, deleteWhenSynced: $0 != currentRoute || !self.active) }
    }
    
    private func uploadRoute(route: Route, deleteWhenSynced: Bool){
        guard let locations = route.fetchUnsavedLocations() where locations.count > 0 else {
            return
        }
        
        _ = API.postObject(.SavePoints(pointsPack: GpsPackModel(locations: locations)))
            .observeOn(Dependencies.sharedDependencies.backgroundWorkScheduler)
            .subscribeNext { (result: ServiceResponseModel) -> Void in
                if result.status != nil {
                    if deleteWhenSynced {
                        Route.delete(route: route, context:self.context)
                    } else {
                        locations.forEach{ $0.saved = true }
                        Route.save(self.context)
                    }
                }
        }
    }
    
    private func createNewRoute() -> Route?{
        let route = Route.addRoute(self.context)
        route?.startTime = NSDate().timeIntervalSinceReferenceDate
        Route.save(self.context)
        return route
    }
}



