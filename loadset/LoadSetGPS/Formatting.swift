//
//  Formatting.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/15/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation


extension Int {
    func format(f: String) -> String {
        return NSString(format: "%\(f)d", self) as String
    }
}

extension Double {
    func format(f: String) -> String {
        return NSString(format: "%\(f)f", self) as String
    }
}

public func += (inout left: NSMutableAttributedString, right: NSAttributedString) {
    left.appendAttributedString(right)
}

