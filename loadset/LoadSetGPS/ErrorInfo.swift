//
//  ErrorInfo.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/23/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import ObjectMapper

public class ErrorInfo: Model {
    
    public private(set) var key: String?
    public private(set) var errorMessage: String?
    
    override public func mapping(map: Map) {
        super.mapping(map)
        key <- map["Key"]
        errorMessage <- map["ErrorMessage"]
    }
}
