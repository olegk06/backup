//
//  SocialViewCell.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/15/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import UIKit


class SocialViewCell: UITableViewCell {    
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
}
