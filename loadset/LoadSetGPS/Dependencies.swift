//
//  Dependencies.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/1/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
#if !RX_NO_MODULE
    import RxSwift
#endif

class Dependencies {
    
    static let sharedDependencies = Dependencies()
    
    let URLSession = NSURLSession.sharedSession()
        let backgroundWorkScheduler: ImmediateSchedulerType
    let mainScheduler: SerialDispatchQueueScheduler
    
    private init() {
        
        let operationQueue = NSOperationQueue()
        operationQueue.maxConcurrentOperationCount = 2
        #if !RX_NO_MODULE
            operationQueue.qualityOfService = NSQualityOfService.UserInitiated
        #endif
        backgroundWorkScheduler = OperationQueueScheduler(operationQueue: operationQueue)
        
        mainScheduler = MainScheduler.instance
    }
    
}

