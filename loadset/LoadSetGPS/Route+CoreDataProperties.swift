//
//  Route+CoreDataProperties.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 1/18/16.
//  Copyright © 2016 Realine. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Route {

    @NSManaged var distance: Int64
    @NSManaged var duration: Int64
    @NSManaged var startTime: NSTimeInterval
    @NSManaged var stopTime: NSTimeInterval
    @NSManaged var user: String?
    @NSManaged var locations: NSSet?

}
