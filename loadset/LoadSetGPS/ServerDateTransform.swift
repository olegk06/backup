//
//  ShieldDateTransform.swift
//
//  Created by Oleg Koshkin on 11/1/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import ObjectMapper

public class ServerDateTransform: TransformType {
    public typealias Object = NSDate
    public typealias JSON = String
    
    public func transformFromJSON(value: AnyObject?) -> NSDate? {
        return ServerDateTransform.formater.dateFromString(value as! String)
    }
    
    public func transformToJSON(value: NSDate?) -> String? {
        if let date = value {
            return ServerDateTransform.formater.stringFromDate(date)
        }
        return nil
    }
    
    static var formater: NSDateFormatter! = {
        let _formater = NSDateFormatter()
        _formater.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        _formater.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return _formater
    }()

}
