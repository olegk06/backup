//
//  StatisticCellView.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 11/10/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import UIKit

class StatisticViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var milesLabel: UILabel!
    @IBOutlet weak var drivingLabel: UILabel!
    @IBOutlet weak var idleLabel: UILabel!
    @IBOutlet weak var privateImage: UIImageView!
    
    @IBOutlet weak var rightGap: NSLayoutConstraint!
    @IBOutlet weak var leftGap: NSLayoutConstraint!
    private var _isPrivate = false
    
    var isPrivate: Bool {
        get { return _isPrivate }
        set {
            _isPrivate = newValue
            self.privateImage.hidden = !_isPrivate
            self.privateImage.image = _isPrivate ? UIImage(named: "ic_eye") : nil
            self.rightGap.constant = _isPrivate ? 5.0 : 2.0
            self.leftGap.constant = _isPrivate ? -3.0 : 0
            setFontColor(_isPrivate ? Theme.service.textColorLightGrey : Theme.service.textColorGrey)
        }
    }
    
    func setData(data: (String, String, String, String)){
        setFontColor(Theme.service.textColorGrey)
        
        dateLabel.text = data.0
        milesLabel.text = data.1
        drivingLabel.text = data.2
        idleLabel.text = data.3
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            let bgColorView = UIView()
            bgColorView.backgroundColor = Theme.service.cellSelectedColor
            self.selectedBackgroundView = bgColorView
            setFontColor(Theme.service.textColorSelected)
        } else {
            setFontColor(_isPrivate ? Theme.service.textColorLightGrey : Theme.service.textColorGrey)
        }
    }
    
    private func setFontColor(color: UIColor){
        dateLabel.textColor = color
        milesLabel.textColor = color
        drivingLabel.textColor = color
        idleLabel.textColor = color
    }
}
