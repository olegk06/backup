//
//  NewUserNotification.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 1/26/16.
//  Copyright © 2016 Realine. All rights reserved.
//

import Foundation
import ObjectMapper

public class NewUserNotification: RealineNotification {
    
    public var transactionId: String?
    public var localMasterId: String?
    public var firstName: String?
    public var lastName: String?
    public var fullName: String?
    public var status: NotificationStatus?
    public var createDate: NSDate?

    
    public override func mapping(map: Map) {
        transactionId <- map["TransactionId"]
        localMasterId <- map["LocalMasterId"]
        firstName <- map["FirstName"]
        lastName <- map["LastName"]
        fullName <- map["FullName"]
    }
}

