//
//  TimeIntervalTransform.swift
//
//  Created by Oleg Koshkin on 11/1/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import Foundation
import ObjectMapper

public class TimeIntervalTransform: TransformType {
    public init() {}
    
    public func transformFromJSON(value: AnyObject?) -> NSTimeInterval? {
        guard let value = value as? Double else {
            return nil
        }
        
        return value/1000
    }
    
    public func transformToJSON(value: NSTimeInterval?) -> String? {
        guard let value = value else {
            return nil
        }
        
        return "\(value.hours.format("02")):\(value.minutes.format("02")):\(value.seconds.format("02"))"
    }
}
