//
//  AppDelegate.swift
//  LoadSet GPS
//
//  Created by Oleg Koshkin on 10/31/15.
//  Copyright © 2015 Pluvio. All rights reserved.
//

import UIKit
import CoreData
import LoginSignUpiOS
import SwiftDate
import RxSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
        
        RLLoginSignUpManager.sharedManager().initForWindow(window) { (configuration: RLManagerConfiguration!) -> Void in
            configuration.settingStoryboard = UIStoryboard.init(name: "Settings", bundle: nil)
            configuration.settingStoryboardID = "GPSSettings"
            configuration.homeItem = SideMenuItem(name:"Dashboard", icon: UIImage.init(named: "ic_home"))
            configuration.homeItem.selectionHandler = {
                RLLoginSignUpManager.sharedManager().openlandingPage()
            }
            configuration.landingScreenSelection = {
                return UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()
            }

            configuration.showLoginToCompany = false
            configuration.allowLoginWithSocialNetworks = false
            configuration.loginImage = UIImage.init(named: "logo")
            configuration.loginAppName = "LoadSet GPS"
            configuration.aboutPageImage = UIImage.init(named: "main_logo")
            configuration.apiAppName = "loadset"
            configuration.apiAppVersion = "1.0"
            configuration.helpContentPageURL = "http://54.187.86.186:9091/gpshelp.html"
            configuration.urlForAppstore = "https://itunes.apple.com/us/app/loadsetgps/id1057015256?ls=1&mt=8&at=10lb2P"
        }
        
        Theme.service.setupBasicUI()

        self.window = window
        self.window!.makeKeyAndVisible()
        
        if Config.service.gpsEnabled {
            Tracker.service.start()
        }
        
        let settings : UIUserNotificationSettings = UIUserNotificationSettings(forTypes: [.Sound, .Alert, .Badge], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        
//        _ = Observable<Int>.interval(10, scheduler: MainScheduler.instance)
//            .subscribeNext { _ in
//                print("Resource count \(RxSwift.resourceCount)")
//        }
        
        return true
    }
    
    func application(application: UIApplication,
        didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings){
            NSNotificationCenter.defaultCenter().postNotificationName(BlackoutZonesChangedNotification, object: nil)
    }
    
    func application(application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        print(deviceToken.description)
        let deviceTokenString: String = deviceToken.description
            .stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
            .stringByReplacingOccurrencesOfString(" ", withString: "") as String
        
        let subscription = PushSubscriptionModel(application: "LoadSet", topic:"GPS topic", token:deviceTokenString, platform: .iOS)
        
        _ = API.postObject(.Subscribe(subscription: subscription))
            .observeOn(Dependencies.sharedDependencies.backgroundWorkScheduler)
            .subscribeNext { (result: ServiceResponseModel) -> Void in }
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
//        print("Recived: \(userInfo)")
//        // For testing :
//        if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
//        {
//            let alertMsg = info["alert"] as! String
//            var alert: UIAlertView!
//            alert = UIAlertView(title: "", message: alertMsg, delegate: nil, cancelButtonTitle: "OK")
//            alert.show()
//        }
    }
    
    //Called if unable to register for APNS.
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }



    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.realine.loadsetgps.LoadSet_GPS" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("LoadSet_GPS", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

