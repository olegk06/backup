//
//  PaddingLabel.swift
//  LoadSetGPS
//
//  Created by Oleg Koshkin on 1/30/16.
//  Copyright © 2016 Realine. All rights reserved.
//

import UIKit

public class PaddingLabel: UILabel {
    public var insets: UIEdgeInsets?
    
    public override func drawTextInRect(rect: CGRect) {
        if let _ = insets {
            super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets!))
        } else {
            super.drawTextInRect(rect)
        }
    }
}
